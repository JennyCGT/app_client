import 'package:flutter/material.dart';
import 'package:app_client/HomeScreen.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:app_client/models/app_state.dart';
import 'package:app_client/redux/reducers.dart';


void main() {
  final _initialState = AppState(cedula: "");
  final Store<AppState> _store = Store<AppState>(reducer, initialState: _initialState);
  runApp(MyApp(store:_store));
}

class MyApp extends StatelessWidget {

  final Store<AppState>store;
  MyApp({this.store});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        
        primaryColor:  Color(0xff00574b),
        // visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    ),
    );
  }
}

