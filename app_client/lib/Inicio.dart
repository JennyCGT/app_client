import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Screen extends StatefulWidget {
  @override
  _Screen createState() => _Screen();
}

class _Screen extends State<Screen> {
  List<bool> isSelected;
  List<bool> cilindro;

  get crossAxisAlignment => null;
  int cantidad = 1;
  int _currentStep = 0;
  List<StepState> _listState;
  int _value = 0;
  var can_controller = new TextEditingController();
  @override
  void initState() {
    isSelected = [
      true,
      false,
    ];
    cilindro = [true, false,false];
    _currentStep = 0;
    _listState = [
      StepState.indexed,
      StepState.complete,
    ];
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(  ),
        body: Theme(
            data: ThemeData(
              primaryColor: Colors.greenAccent[700],
            ),
            child: Container(
                padding: const EdgeInsets.only(top: 10.0),
                child: ListView(
                  children: <Widget>[
                    Container(
                      // padding: const EdgeInsets.symmetric(),
                      alignment: Alignment.center,
                      height: 40,
                      child: ToggleButtons(
                        children: <Widget>[
                          Text('  Ocupado  '),
                          Text(' Disponible  '),
                        ],
                        // constraints: BoxConstraints(maxHeight: 105),
                        borderRadius: BorderRadius.circular(15),
                        borderWidth: 2,
                        selectedColor: Colors.white,

                        selectedBorderColor: isSelected[0]
                            ? Colors.redAccent[100]
                            : Colors.greenAccent[700],
                        fillColor: isSelected[0]
                            ? Colors.redAccent[100]
                            : Colors.greenAccent[700],
                        borderColor: isSelected[0]
                            ? Colors.redAccent[100]
                            : Colors.greenAccent[700],
                        color: isSelected[0]
                            ? Colors.redAccent[100]
                            : Colors.greenAccent[700],

                        onPressed: (int index) {
                          setState(() {
                            for (int buttonIndex = 0;
                                buttonIndex < isSelected.length;
                                buttonIndex++) {
                              if (buttonIndex == index) {
                                isSelected[buttonIndex] = true;
                              } else {
                                isSelected[buttonIndex] = false;
                              }
                            }
                          });
                        },
                        isSelected: isSelected,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      constraints: BoxConstraints.expand(height: 300),
                      color: Colors.white,
                      child: Stepper(
                          type: StepperType.horizontal,
                          currentStep: _currentStep,
                          controlsBuilder: (BuildContext context,
                              {VoidCallback onStepContinue,
                              VoidCallback onStepCancel}) {
                            return Row(
                              children: <Widget>[
                                Container(
                                  child: null,
                                ),
                                Container(
                                  child: null,
                                ),
                              ],
                            );
                          },
                          onStepContinue: () {
                            setState(() {
                              _currentStep++;
                            });
                          },
                          steps: [
                            //STEP1
                            Step(
                              state: _currentStep == 0
                                  ? _listState[0]
                                  : _currentStep > 0
                                      ? _listState[1]
                                      : _listState[0],
                              isActive: _currentStep == 0
                                  ? true
                                  : _currentStep > 0 ? true : false,
                              title: Text(
                                "En\nEspera",
                                textAlign: TextAlign.center,
                              ),
                              content: Container(
                                  child: Column(
                                children: <Widget>[
                                  Text(
                                    "En Espera...",
                                    textAlign: TextAlign.center,
                                  ),
                                  Center(
                                    child: RaisedButton(
                                      child: Text('siguiente'),
                                      onPressed: () {
                                        setState(() {
                                          _currentStep++;
                                        });
                                      },
                                    ),
                                  )
                                ],
                              )),
                            ),
                            //STEP2
                            Step(
                              state: _currentStep == 1
                                  ? _listState[0]
                                  : _currentStep > 1
                                      ? _listState[1]
                                      : _listState[0],
                              isActive: _currentStep == 1
                                  ? true
                                  : _currentStep > 1 ? true : false,
                              title: Text(
                                "Buscando\nPedido",
                                textAlign: TextAlign.center,
                              ),
                              content: Container(
                                  child: Column(
                                children: <Widget>[
                                  RaisedButton(
                                    child: Text('PEDIDOS'),
                                    onPressed: () {
                                      pedidoDialog(context);
                                    },
                                  ),
                                  RaisedButton(
                                      child: Text('siguiente'),
                                      onPressed: () {
                                        setState(() {
                                          _currentStep++;
                                        });
                                      }),
                                  RaisedButton(
                                    child: Text('atras'),
                                    onPressed: () {
                                      setState(() {
                                        _currentStep--;
                                      });
                                    },
                                  ),
                                ],
                              )),
                            ),
                            //STEP3
                            Step(
                              title: Text(
                                "Entregando\n",
                                textAlign: TextAlign.center,
                              ),
                              content: Container(
                                  child: Column(
                                children: <Widget>[
                                  Text(
                                    'ENTREGA ESTE PEDIDO',
                                    textAlign: TextAlign.center,
                                  ),
                                  RaisedButton(
                                    child: Text('CANCELACIÓN'),
                                    onPressed: () {},
                                  ),
                                  RaisedButton(
                                      child: Text('siguiente'),
                                      onPressed: () {
                                        setState(() {
                                          _currentStep++;
                                        });
                                      }),
                                  RaisedButton(
                                    child: Text('atras'),
                                    onPressed: () {
                                      setState(() {
                                        _currentStep--;
                                      });
                                    },
                                  ),
                                ],
                              )),
                              state: _currentStep == 2
                                  ? _listState[0]
                                  : _currentStep > 2
                                      ? _listState[1]
                                      : _listState[0],
                              isActive: _currentStep == 2
                                  ? true
                                  : _currentStep > 2 ? true : false,
                            ),
                            //STEP4
                            Step(
                              title: Text(
                                "Confirmación\nde entrega",
                                textAlign: TextAlign.center,
                              ),
                              content: Container(
                                  child: Column(
                                children: <Widget>[
                                  Text(
                                    'ENTREGA COMPLETA\nGRACIAS POR TU AMABILIDAD',
                                    textAlign: TextAlign.center,
                                  ),
                                  RaisedButton(
                                    child: Text('atras'),
                                    onPressed: () {
                                      setState(() {
                                        _currentStep--;
                                      });
                                    },
                                  ),
                                ],
                              )),
                              state: _currentStep == 3
                                  ? _listState[0]
                                  : _currentStep > 3
                                      ? _listState[1]
                                      : _listState[0],
                              isActive: _currentStep == 3
                                  ? true
                                  : _currentStep > 3 ? true : false,
                            ),
                          ]),
                    ),
                  ],
                ))));
  }

  void pedidoDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return(StatefulBuilder(builder: (context,setState){
        return AlertDialog(
          title: Text('Hacer un nuevo pedido'),
          content: Container(
            height: 200,
            child: ListView(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          cantidad++;
                          can_controller.text = cantidad.toString();
                        });
                      },
                      child: Icon(Icons.add),
                    ),
                    Container(
                      width: 40,
                      child: TextFormField(
                        controller: can_controller,
                        decoration: InputDecoration(
                            hintText: cantidad.toString(),
                            labelStyle: TextStyle(color: Colors.grey),
                            focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.amber, width: 2))),
                        cursorColor: Colors.amber,
                        keyboardType: TextInputType.text,
                        textAlign: TextAlign.center,
                        onChanged: (value) {
                          setState(() {
                            cantidad = int.parse(can_controller.text);
                          });
                        },
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          cantidad--;
                          if(cantidad<1){
                          cantidad=1;
                          }
                          print("NUMERO DE CILINDROS $cantidad");
                          can_controller.text = cantidad.toString();
                        });
                      },
                      child: Icon(Icons.maximize),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                ToggleButtons(
                borderColor: Colors.black,
                fillColor: Colors.grey,
                borderWidth: 2,
                selectedBorderColor: Colors.black,
                selectedColor: Colors.white,
                borderRadius: BorderRadius.circular(0),
                children: <Widget>[
                    Container(
                      height: 120,
                        width: 60,
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset('assets/cilidro-de-gas-01.png')
                    ),
                    Container(
                      height: 120,
                        width: 60,
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset('assets/cilidro-de-gas-02.png')
                    ),
                    Container(
                      height: 120,
                        width: 60,
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset('assets/cilidro-de-gas-03.png')
                    ),
                ],
                onPressed: (int index) {
                    setState(() {
                    for (int i = 0; i < cilindro.length; i++) {
                         cilindro[i] = i == index;
                    }
                    });
                },
                isSelected: cilindro,
                ),
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            Row(
              children: <Widget>[
            FlatButton(
              child: Text('Hacer Pedido'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

              ],
            ),
          ],
        );

        }));
      },
    );

  }
}
