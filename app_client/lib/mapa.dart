import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'dart:async';
import 'package:flutter/services.dart';

// import 'components/constant.dart';

const double CAMERA_ZOOM = 15;
const double CAMERA_TILT = 10;
const double CAMERA_BEARING = 0;
const LatLng SOURCE_LOCATION = LatLng(-0.256, 0.00);// -78.5378912);
const LatLng DEST_LOCATION = LatLng(-0.2662, -78.5359825);
const LatLng RIVAL_LOCATION = LatLng(-0.2662, -78.54281925);

class Screen extends StatefulWidget {
  @override
  _Screen createState() => _Screen();
}

class _Screen extends State<Screen> {

 // Controler y variable del Mapa
  Completer<GoogleMapController> _controller = Completer(); 
  LocationData currentLocation;
  LocationData destinationLocation;
  Location location; 
  double pinPillPosition = -100;
  CameraPosition _position;
  Set<Marker> _markers={};
  Set<Polyline> _polylines ={};
  Set<Circle> _circle={};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  
  // Icons  
  BitmapDescriptor sourceIcon;
  BitmapDescriptor destinationIcon;
  BitmapDescriptor rivalIcon;

  @override
  void initState() {
    super.initState();
    _getUserLocation();
    // set the initial location
    // // setSourceAndDestinationIcons();
    setSourceAndDestinationIcons();
   setInitialLocation(); 

  }
   Future <void> _getUserLocation() async{
    location = new Location();
    location.onLocationChanged.listen((LocationData cloc) {//async{ 
      currentLocation = cloc;
      print("Posicion listen:       $currentLocation");
    _updatePinonMap();
   });
}
  Future <void> setSourceAndDestinationIcons() async {
    sourceIcon = await BitmapDescriptor.fromAssetImage(
    // sourceIcon = awaBitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 2.5),
      'assets/logos2-05.png');

    destinationIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 2.5),
      'assets/logos-para-mapas-06-1.png');

    rivalIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 2.5),
      'assets/logos-para-mapas-05-1.png');
  }  
 
  Future <void> setInitialLocation() async {
    // print("Posicion intial:       $currentLocation");    
     currentLocation = await location.getLocation();
      destinationLocation = LocationData.fromMap({
      "latitude": DEST_LOCATION.latitude,
      "longitude": DEST_LOCATION.longitude
    });   
  }
  

  @override
  Widget build(BuildContext context) {
    // print("Posicion Build:       $currentLocation");
    CameraPosition initialCameraPosition = CameraPosition(
        zoom: CAMERA_ZOOM,
        tilt: CAMERA_TILT,
        bearing: CAMERA_BEARING,
        target: SOURCE_LOCATION);
    if (currentLocation != null) {
      initialCameraPosition = CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: CAMERA_ZOOM,
          tilt: CAMERA_TILT,
          bearing: CAMERA_BEARING
      );
    }
    _position=initialCameraPosition;

    return Scaffold(
        body:Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 550,
            // padding: EdgeInsets.only(top:20.00),
            margin: EdgeInsets.only(top:20.00),
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child:
            GoogleMap(
              // markers: Set<Marker>.of(markers.values),
              // polylines: Set<Polyline>.of(polylines.values),
              markers: _markers,
              // polylines: _polylines,
              myLocationButtonEnabled: false,              
              myLocationEnabled: true,
              compassEnabled: true,
              circles: _circle,
              tiltGesturesEnabled: false,
              zoomGesturesEnabled: true,
              // zoomControlsEnabled: true,
              scrollGesturesEnabled: true,
              onCameraMove: (position) {
                setState(() {
                  _position =position;
                });
              },
              mapType: MapType.normal,
              initialCameraPosition: initialCameraPosition,
               onTap: (LatLng loc) {
                pinPillPosition = -100;
              },
              onMapCreated: onMapCreated,
              // onMapCreated: (GoogleMapController controller) {
              //   _controller.complete(controller);
              // }),
          )),

        ],
    ));
  }
  void onMapCreated(GoogleMapController controller){
    _controller.complete(controller);
    setMapPind();
    // setPolyline();

  }

  void setMapPind(){
    setState(() {
      _markers.add(Marker(markerId: MarkerId('sourcePin'),
      position: SOURCE_LOCATION,icon: sourceIcon));
      _markers.add(Marker(markerId: MarkerId('destPin'),
      position: DEST_LOCATION, icon: destinationIcon));
      _markers.add(Marker(markerId: MarkerId('rivalePin'),
      position: RIVAL_LOCATION,icon: rivalIcon));
      _circle.add(Circle(circleId:CircleId('sourceCircle'),radius: 10.00,
      zIndex: -1, strokeColor: Colors.blueAccent, center: LatLng(0,0)));
    });
  }

  void _updatePinonMap () async{
    try{
   if (!mounted) return;
    CameraPosition cPosition = CameraPosition(
   zoom: CAMERA_ZOOM,
  //  tilt: CAMERA_TILT,
  //  bearing: CAMERA_BEARING,
   target: LatLng(currentLocation.latitude, 
      currentLocation.longitude),
   );
  // final GoogleMapController controller = _controller;
  final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
   // do this inside the setState() so Flutter gets notified
   // that a widget update is due
   if (!mounted) return;
   setState(() {
      // updated position
      var pinPosition = LatLng(currentLocation.latitude,
      currentLocation.longitude);
      
      // the trick is to remove the marker (by id)
      // and add it again at the updated location
      _markers.removeWhere(
      (m) => m.markerId.value == 'sourcePin');
      _markers.add(Marker(
         markerId: MarkerId('sourcePin'),
         position: pinPosition, // updated position
         icon: sourceIcon
      ));
      _circle.removeWhere((m) => m.circleId.value == 'circlePin');

      _circle.add(Circle(
      circleId: CircleId('circlePin'),
      radius: 500.00,
      zIndex: -1,
      strokeColor: Colors.blueAccent,
      strokeWidth: 1,
      center: pinPosition,
      fillColor: Colors.blue.withAlpha(70))
      );
      
   });

    }on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }

}
  
  // setPolyline() async {
  //   PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
  //       keyApi,
  //       PointLatLng(SOURCE_LOCATION.latitude, SOURCE_LOCATION.longitude),
  //       PointLatLng(DEST_LOCATION.latitude, DEST_LOCATION.longitude),
  //       travelMode: TravelMode.driving,        
  //     // wayPoints: [PolylineWayPoint(location: "Casita, Cris")]
  //   );
  //   print("holiiiiiiiiiiiiiiii");
  //   print(result.points.length);
  //   if (result.points.isNotEmpty) {
  //     print('entreeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee');
  //     result.points.forEach((PointLatLng point) {
  //       polylineCoordinates.add(LatLng(point.latitude, point.longitude));
  //     });
  //   }
  //   setState(() {
  //     Polyline polyline= Polyline(polylineId: PolylineId("pruebaaa"),
  //     color: Colors.red, points: polylineCoordinates
  //     );
  //     _polylines.add(polyline);
  //   });
  // }
}