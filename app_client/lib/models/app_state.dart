import 'package:flutter/material.dart';

class AppState{
  String cedula='';
  String contrasena='';

  String name='';
  String id='';
  String mobile='';
  String jtoken='';
  AppState(
    {@required this.cedula, this.contrasena });
  
  AppState.fromAppState( AppState another){
    cedula = another.cedula;
    contrasena= another.contrasena;
    name = another.name;
    id = another.id;
    mobile = another.mobile;
  }
  String get view_cedula => cedula;
  String get view_Contrasena => contrasena;
  String get view_name => name;
  String get view_id => id;
  String get view_mobile => mobile;


}