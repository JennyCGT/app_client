import 'dart:io';

import 'package:app_client/models/app_state.dart';
import 'package:flutter/material.dart';
import 'package:app_client/HomeScreen.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:app_client/models/app_state.dart' as app_state;
import 'package:app_client/redux/actions.dart';
import 'package:http/http.dart';
import 'dart:async';
import 'dart:convert';

class FormRegister extends StatefulWidget {
  FormRegister({Key key}) : super(key: key);
  @override
  _FormRegister createState() => _FormRegister();
}


class _FormRegister extends State<FormRegister> {
  final myController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _autoValidate = false;
  String _name;
  String _id;
  String _mobile;
  String _code_val;
  int status_patch;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      //    If all data are correct  then save data to out variables
      _formKey.currentState.save();
      _makePatchRequest();

      // Navigator.push(context,MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      setState(() {
        this._autoValidate = true;
        // _autoValidate = true;
      });
    }
  }

  Future<void> _makePatchRequest() async {
    try{
      String url = 'http://54.70.216.182:80/EgmSystems/Registro/newUsr';
      Map<String, String> headers = {"Content-type": "application/json"};
      String json_ser =
          '{ "scope":"clickgas-platform-users","cedula":"$_id",  "telefono":"$_mobile", "nombre":"$_name",  "device": "movil"}';
      // make PATCH request
      print('haciendo verificacion');
      Response response = await patch(url, headers: headers, body: json_ser);
      int statusCode = response.statusCode;
      status_patch = statusCode;
      final body = json.decode(response.body);
      print(statusCode);
      print("Respuestaaaa del web server");
      print(response.body);
      _showDialog();
    } on SocketException catch (e) {
      status_patch = 1000;
      print('No internet');
      print(status_patch);
    } on Error catch (e) {
      status_patch = 2000;
    } on TimeoutException catch (e) {
      status_patch = 2000;
    }

  }
  void _showDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Registro'),
            content: status_patch == 200
                ? Text('Registrado con éxito')
                : status_patch == 202
                    ? Text("Este Usuario ya esta registrado")
                    : Text('Intente despues'),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()));
                  },
                  child: Text('Salir'))
            ],
          );
        });
  }

  Widget build(BuildContext context) {
    var validator2 = (value) {
      if (value.isEmpty) {
        return 'Llene todos los campos';
      }
      if (value.length != 10) {
        return 'Cédula no válida';
      }
    };
    var validator1 = (value) {
      if (value.isEmpty) {
        return 'Llene todos los campos';
      }
      if (value.length != 10 || !value.startsWith('09')) {
        return 'Número celular no válido';
      }
    };
    return Form(
        key: _formKey,
        child: Scaffold(
            // backgroundColor: Color(0xff00574b),
            // appBar: AppBar( ),
            body: StoreConnector<app_state.AppState, app_state.AppState>(
          converter: (store) => store.state,
          builder: (context, state) {
            return Container(
                // padding: const EdgeInsets.only(top: 30.0),
                child: ListView(children: <Widget>[
              Container(
                  // color: Color(0xff00574b),
                  width: MediaQuery.of(context).size.width,
                  height: 120.00,
                  decoration: new BoxDecoration(
                    color: Color(0xff105d55),
                    image: new DecorationImage(
                      image: ExactAssetImage('assets/logos-02-1.png'),
                      fit: BoxFit.contain,
                    ),
                  )),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 25)
                    .copyWith(bottom: 10, top: 30),
                child: Text(
                  "REGISTRO PARA NUEVOS USUARIOS",
                  style: TextStyle(
                    fontSize: 18.00,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25)
                    .copyWith(bottom: 10),
                child: TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Llene todos los campos';
                    }
                  },
                  onSaved: (String value) {
                    _name = value;
                  },
                  onChanged: (value) {
                    StoreProvider.of<app_state.AppState>(context).dispatch(Name(value));
                  },
                  decoration: InputDecoration(
                      labelText: "Nombre",
                      labelStyle: TextStyle(color: Color(0xff00574b)),
                      // prefixIcon: const Icon(Icons.account_circle, color: Color(0xff00574b),),
                      focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xff00574b), width: 2))),
                  initialValue: '${state.name}',
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                  cursorColor: Colors.green[900],
                  keyboardType: TextInputType.text,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25)
                    .copyWith(bottom: 10),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "Cédula",
                    labelStyle: TextStyle(color: Color(0xff00574b)),
                    // prefixIcon: const Icon(Icons.perm_identity, color: Color(0xff00574b),),
                    focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff00574b), width: 2)),
                  ),
                  initialValue: "${state.id}",
                  validator: validator2,
                  onSaved: (String value) {
                    _id = value;
                  },
                  onChanged: (value) {
                    StoreProvider.of<AppState>(context)
                        .dispatch(Id(value));
                  },
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                  cursorColor: Colors.green[900],
                  keyboardType: TextInputType.phone,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25)
                    .copyWith(bottom: 20),
                child: TextFormField(
                  validator: validator1,
                  onSaved: (String value) {
                    _mobile = value;
                  },
                  onChanged: (value) {
                    StoreProvider.of<app_state.AppState>(context)
                        .dispatch(Mobile(value));
                  },
                  decoration: InputDecoration(
                      labelText: "Celular",
                      labelStyle: TextStyle(color: Color(0xff00574b)),
                      // prefixIcon: const Icon(Icons.local_phone, color: Color(0xff00574b),),
                      focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xff00574b), width: 2)),),
                  initialValue: '${state.mobile}',
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                  cursorColor: Colors.green[900],
                  keyboardType: TextInputType.phone,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 75)
                    .copyWith(bottom: 20),
                child: RaisedButton(
                  onPressed: () {
                    _validateInputs();
                  },
                  child: Text("Registrar"),
                ),
              ),
            ]));
          },
        )));
  }
}
