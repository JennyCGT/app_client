import 'dart:async';
import 'dart:io';

import 'package:app_client/models/app_state.dart';
import 'package:flutter/material.dart';
import 'package:app_client/Form.dart';
import 'package:app_client/principal.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:app_client/models/app_state.dart' as app_state;
import 'package:app_client/redux/actions.dart';
import 'package:http/http.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  final myController = TextEditingController();
  final passController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autoValidate = false;
  String _cedula;
  String _contrasena;
  int status_patch;
  int pass_status_patch = 0;
  bool id_validate = false;
  String token;

  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  Future<void> _idPatchRequest(url, headers, json_id) async {
    print('haciendo verificacion');
    try {
      Response response = await patch(url, headers: headers, body: json_id);
      int statusCode = response.statusCode;
      final body = json.decode(response.body);
      status_patch = statusCode;
      print(statusCode);
      print(response.body);
      if (status_patch == 200 || status_patch == 401) {
        _showdialog();
        if (body['estado'] == "modulo inexistente") {
          id_validate = true;
        } else {
          id_validate = false;
        }
      }
    } on SocketException catch (e) {
      status_patch = 1000;
      _user_error();
      print('No internet');
      print(status_patch);
    } on Error catch (e) {
      status_patch = 2000;
    } on TimeoutException catch (e) {
      status_patch = 2000;
    }
  }

  Future<void> _pass_validRequest(url_val, headers, json_val) async {
    try {
      Response response =
          await patch(url_val, headers: headers, body: json_val);
      int statusCode = response.statusCode;
      final body = json.decode(response.body);
      print("Validando cuenta");
      status_patch = statusCode;
      print(statusCode);
      print(response.body);
      if (status_patch == 200) {
        _passPatchRequest();
      } else {
        _pass_error();
      Navigator.of(context).pop();
      }
    } on SocketException catch (e) {
      status_patch = 1000;
      _pass_error();
      Navigator.of(context).pop();
    } on Error catch (e) {
      status_patch = 2000;
      _pass_error();
      Navigator.of(context).pop();
    } on TimeoutException catch (e) {
      status_patch = 2000;
      _pass_error();
      Navigator.of(context).pop();
    }
  }

  Future<void> _passPatchRequest() async {
    try {
      String url_auth = 'http://54.70.216.182:80/EgmSystems/Auth/getAuthMob';
      Map<String, String> headers = {"Content-type": "application/json"};
      String json_auth =
          '{ "username":"${myController.text}","scope":"clickgas-platform-users","password": "${passController.text}"}';
      // make PATCH request
      print('Validando Contrasena');
      Response response = await patch(url_auth, headers: headers, body: json_auth);
      pass_status_patch = response.statusCode.toInt();
      if (pass_status_patch == 200) {
        final body_auth = json.decode(response.body);
        token = body_auth["jtoken"];
        StoreProvider.of<app_state.AppState>(context).dispatch(Token(token));
        print(token);
        Navigator.of(context).pop();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Principal()));
      } else if (pass_status_patch == 401) {
        _pass_error();
        Navigator.of(context).pop();
      } else {
        _pass_error();
        Navigator.of(context).pop();
      }
    } on SocketException catch (e) {
      status_patch = 1000;
      _pass_error();
      Navigator.of(context).pop();
    } on Error catch (e) {
      status_patch = 2000;
      _pass_error();
      Navigator.of(context).pop();
    } on TimeoutException catch (e) {
      status_patch = 2000;
      _pass_error();
      Navigator.of(context).pop();
    }
  }

  void _validate_cedula() {
    print("La cedula es ${myController.text}");
    if(myController.text.length!=10){
      _user_invalid();
    }else{
    String url = 'http://54.70.216.182:80/EgmSystems/Auth/getAuthMob';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json_id =
        '{ "username":"${myController.text}","scope":"clickgas-platform-users"}';
    _idPatchRequest(url, headers, json_id);
    // _showdialog();
    }
  }

  void _validate_contrasena() {
    print("La cedula es ${passController.text}");
    String url_val = 'http://54.70.216.182:80/EgmSystems/Registro/verifUsr';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json_val =
        '{"scope":"clickgas-platform-users", "cedula":"${myController.text}","device": "movil","codigoVerificacion": "${passController.text}"}';
    if (id_validate) {
      _pass_validRequest(url_val, headers, json_val);
    }
    _passPatchRequest();
  }

  void _user_invalid(){
    SnackBar snackBar = new SnackBar(
        content: new Text("Cedula no válida"), duration: Duration(seconds: 2));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
  void _user_error() {
    String context;
    if (status_patch == 1000) {
      context = "Sin Conexión a Internet";
    } else if (status_patch == 2000) {
      context = "Intentelo mas Tarde";
    } else {
      context = "Usuario no Registrado";
    }
    SnackBar snackBar = new SnackBar(
        content: new Text(context), duration: Duration(seconds: 2));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _pass_error() {
    String context;
    if (status_patch == 1000) {
      context = "Sin Conexión a Internet";
    } else if (status_patch == 2000) {
      context = "Intentelo mas Tarde";
    } else {
      context = "Contraseña Incorrecta";
    }
    SnackBar snackBar = new SnackBar(
        content: new Text(context), duration: Duration(seconds: 2));
    _scaffoldKey.currentState.showSnackBar(snackBar);
    // // _dialogKey.currentContext
    // _dialogKey.currentState.showSnackBar(snackBar);
  }

  void _showdialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12))),
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: Color(0xff105d55),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(12.0),
                      topRight: Radius.circular(12.0),
                    )),
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 20.0, bottom: 20),
                child: Text(
                  "Ingrese su contraseña",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18.00, color: Colors.white),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                    top: 20.0, bottom: 20.0, left: 15, right: 15),
                color: Colors.grey[350].withOpacity(0.5),
                child: TextFormField(
                  decoration: InputDecoration(
                      labelText: "Contraseña",
                      labelStyle: TextStyle(color: Colors.grey),
                      prefixIcon: const Icon(
                        Icons.lock,
                        color: Color(0xff00574b),
                      ),
                      focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.amber, width: 2))),
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                  cursorColor: Colors.amber,
                  keyboardType: TextInputType.phone,
                  obscureText: true,
                  controller: passController,
                  onChanged: (value) {
                    StoreProvider.of<AppState>(context)
                        .dispatch(Password(value));
                  },
                ),
              ),
              Container(
                margin:
                    EdgeInsets.only(top: 20, bottom: 50, left: 25, right: 25),
                child: RaisedButton(
                    onPressed: () {
                      _validate_contrasena();
                    },
                    child: Text(
                      " Ingresar",
                      style: TextStyle(fontSize: 18.00),
                    )),
              ),
            ]),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var validator2 = (value) {
      if (value.isEmpty) {
        print("Cedula vacia");
        return 'Llene todos los campos';
      }
      if (value.length != 10) {
        print('Cedula no valida');
        return 'Cédula no válida';
      }
    };
    return Container(
        decoration: BoxDecoration(
          image: new DecorationImage(
            image: ExactAssetImage('assets/back4.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.transparent,
          body: StoreConnector<app_state.AppState, app_state.AppState>(
            converter: (store) => store.state,
            builder: (context, state) {
              return Container(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: ListView(
                    children: <Widget>[
                      Container(
                          width: 200.00,
                          height: 220.00,
                          decoration: new BoxDecoration(
                            image: new DecorationImage(
                              image: ExactAssetImage('assets/logos2-05.png'),
                              fit: BoxFit.fitHeight,
                            ),
                          )),
                      Card(
                          margin: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 45),
                          color: Colors.grey[350].withOpacity(0.5),
                          child: Column(children: <Widget>[
                            TextFormField(
                              decoration: InputDecoration(
                                  labelText: "Usuario / Cédula",
                                  labelStyle: TextStyle(color: Colors.grey),
                                  prefixIcon: const Icon(
                                    Icons.account_circle,
                                    color: Color(0xff00574b),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.amber, width: 2))),
                              controller: myController,
                              // initialValue: '${state.cedula}',
                              style: TextStyle(
                                  color: Colors.black, fontSize: 16.0),
                              cursorColor: Colors.amber,
                              keyboardType: TextInputType.phone,
                              onChanged: (value) {
                                StoreProvider.of<AppState>(context)
                                    .dispatch(Users(value));
                              },
                            ),
                          ])),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 75)
                                .copyWith(bottom: 40, top: 20),
                        child: RaisedButton(
                          onPressed: () {
                            _validate_cedula();
                          },
                          // _showdialog();
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => Principal()));
                          child: Text(
                            "Ingresar",
                            style: TextStyle(fontSize: 18.00),
                          ),
                        ),
                      ),
                      Text(
                        "¿ No tienes una cuenta? ",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18.00, color: Colors.black),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 75)
                                .copyWith(top: 15),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormRegister()));
                          },
                          child: Text(
                            "Registrar",
                            style: TextStyle(fontSize: 18.00),
                          ),
                        ),
                      ),
                    ],
                  ));
            },
          ),
        ));
  }
}
