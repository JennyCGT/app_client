import 'package:flutter/material.dart';
import 'package:app_client/principal.dart' as principal;
import 'package:app_client/Aboutus.dart' as about;


class Drawerprincipal extends StatefulWidget {
  @override
  _Drawerprincipal createState() => _Drawerprincipal();
}

class _Drawerprincipal extends State<Drawerprincipal> {
  get crossAxisAlignment => null;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(
                  color: Color(0xff00574b),
                ),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Material(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        //elevation: 10,
                        child: Container(
                          height: 90,
                          padding: EdgeInsets.all(2.0),
                          child: Image.asset(
                            'assets/logos2-05.png',
                            scale: 1.5, fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'ClickGas',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      // Padding(
                      //   padding: EdgeInsets.all(8.0),
                      //   child: Text(
                      //     'ClickGas',
                      //     style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 20,
                      //     ),
                      //   ),
                      // )

                    ],
                  ),
                )),
            ListTile(
              leading: Icon(Icons.apps),
              title: Text(
                'Menú Principal',
                style: TextStyle(fontSize: 16.0),
              ),
              onTap: () {
                Navigator.of(context).pop(principal.Principal());
               // Navigator.of(context).pushReplacementNamed('principal');
              },
            ),

            new Divider(
              color: Colors.black45,
              indent: 16.0,
            ),
            ListTile(
              leading: Icon(Icons.payment),
              title: Text(
                'Pagos',
                style: TextStyle(fontSize: 16.0),
              ),
              onTap: () {},
            ),
             new Divider(
              color: Colors.black45,
              indent: 16.0,
            ),
            ListTile(
              leading: Icon(Icons.info),
              title: Text(
                'Acerca de nosotros',
                style: TextStyle(fontSize: 16.0),
              ),
              onTap: () {
               Navigator.of(context).pop('about.Aboutus');
               
              },
            ),
          ],
        ),
    );
  }
}
