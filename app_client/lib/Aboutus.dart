import 'package:flutter/material.dart';
import 'package:app_client/principal.dart';

import 'package:app_client/drawe_principal.dart' as drawer;

class Aboutus extends StatefulWidget {

  @override
  _Aboutus createState() => _Aboutus();
}

class _Aboutus extends State<Aboutus> {
 
  get crossAxisAlignment => null;
  @override

 Widget build(BuildContext context) {
    return MaterialApp(
      home: Container(

        child: Scaffold(
          drawer: Drawer(
            child: drawer.Drawerprincipal(),
          ),
          appBar: AppBar(
            backgroundColor: Color(0xff00574b),
            //  centerTitle: true,
            title: const Text('ClickGas'),
            ),
            
      body: Container(        
        padding: const EdgeInsets.only(top: 30.0),
          child: ListView(          
          children:<Widget>[  
          // Text(msg),                
            Card(
              margin:const EdgeInsets.only(left: 15.0, right: 15.0,top: 45),
              color: Colors.grey[350].withOpacity(0.5),
              child: Column(
                children:<Widget>[
                  
                TextFormField(
                decoration: InputDecoration(
                labelText: "Usuario / Cédula", labelStyle: TextStyle(color:Colors.grey),
                prefixIcon: const Icon(Icons.account_circle, color: Color(0xff00574b),),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber, width: 2))),
                style: TextStyle(color:Colors.black, fontSize: 16.0),
                cursorColor: Colors.amber,
                keyboardType: TextInputType.phone,                 
                ),  
                TextFormField(
                decoration: InputDecoration(                
                labelText: "Contraseña", labelStyle: TextStyle(color:Colors.grey),
                prefixIcon: const Icon(Icons.lock, color: Color(0xff00574b),),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.amber, width: 2))),
                style: TextStyle(color:Colors.black , fontSize: 16.0),
                cursorColor: Colors.amber,
                keyboardType: TextInputType.text,
                obscureText: true,
                ),
                ]
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 75).copyWith(bottom: 40,top: 20),
              child:
                  RaisedButton(onPressed: (){
                    Navigator.push(context,
                    MaterialPageRoute(builder: (context)=>Principal())
                    );},
                  child: Text("Ingresar",style: TextStyle(fontSize:18.00),),
                  
                  ),
            ),         

           
        ],  
        )      
      )
      
          ),
      )
    );
 }
 }
