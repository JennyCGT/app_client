
class Users{
  final String payload;
  Users(this.payload);
}

class Password{
  final String payload;
  Password(this.payload);
}

class Name{
  final String payload;
  Name(this.payload);
}

class Id{
  final String payload;
  Id(this.payload);
}

class Mobile{
  final String payload;
  Mobile(this.payload);
}

class Token{
  final String payload;
  Token(this.payload);
}
