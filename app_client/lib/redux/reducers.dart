
import 'package:app_client/models/app_state.dart';
import 'package:app_client/redux/actions.dart';
import 'package:redux/redux.dart';

AppState reducer(AppState prevState, dynamic action){
  AppState newState= AppState.fromAppState(prevState);    
  if (action is Users){
    newState.cedula = action.payload;
  }
  else if (action is Password){
    newState.contrasena = action.payload;
  }
  else if (action is Name){
    newState.name = action.payload;
  }
  else if (action is Id){
    newState.id = action.payload;
  }
  else if (action is Mobile){
    newState.mobile = action.payload;
  }
  else if( action is Token){
    newState.jtoken = action.payload;
  }

  return newState;

}