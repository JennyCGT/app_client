import 'package:flutter/material.dart';

class Pedidos extends StatefulWidget{
  _Pedidos createState()=> _Pedidos();
}

class _Pedidos extends State<Pedidos>{
  var can_controller = new TextEditingController();
  int cantidad = 1;
  String cilindro = "";
  int _value=0;

 
// user defined function
// user defined function
  void pedidoDialog(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text('Hacer un nuevo pedido'),
          content: Container(
            height: 200,
            child: ListView(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          cantidad++;
                          can_controller.text = cantidad.toString();
                        });
                      },
                      child: Icon(Icons.add),
                    ),
                    Container(
                      width: 40,
                      child: TextFormField(
                        controller: can_controller,
                        decoration: InputDecoration(
                            labelStyle: TextStyle(color: Colors.grey),
                            focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.amber, width: 2))),
                        cursorColor: Colors.amber,
                        keyboardType: TextInputType.text,
                        textAlign: TextAlign.center,
                        onChanged: (value) {
                          setState(() {
                            cantidad = int.parse(can_controller.text);
                          });
                        },
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if(cantidad<=0){
                          cantidad=0;
                          }else{
                            cantidad--;
                          }
                          can_controller.text = cantidad.toString();
                        });
                      },
                      child: Icon(Icons.maximize),
                    ),
                  ],
                ),
                SizedBox(
                  width: 5,
                  height: 40,
                ),
                Row(
                  children: <Widget>[
                    GestureDetector(
                      child: Container(
                        height: 120,
                        width: 60,
                        child: Image.asset('cilidro-de-gas-01.png'),
                        decoration: BoxDecoration(color: _value == 0 ? Colors.grey : Colors.transparent, )
                        // color: _value == 0 ? Colors.grey : Colors.transparent,
                      ),
                      onTap: () => setState(() => _value = 0),
                    ),
                    SizedBox(width: 20),
                    GestureDetector(
                      child: Container(
                        height: 120,
                        width: 60,
                        child: Image.asset('cilidro-de-gas-02.png'),
                        decoration: BoxDecoration(color: _value == 1 ? Colors.grey : Colors.transparent, )
                      ),
                      onTap: () => setState(() => _value = 1),
                    ),
                    SizedBox(width: 20),
                    GestureDetector(
                      child: Container(
                        height: 120,
                        width: 60,
                        child: Image.asset('cilidro-de-gas-03.png'),
                        decoration: BoxDecoration(color: _value == 2 ? Colors.grey : Colors.transparent, )
                      ),
                      onTap: () => setState(() => _value = 2),
                    ),
                  ],
                ),
                Row(
                  children:<Widget>[
                  Text(can_controller.text),
                  Text(_value.toString()),
                  ]
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    pedidoDialog(context);
    return Container(
      
    );
  }  


}
// // user defined function
//   void pedidoDialog(BuildContext context) {
//     // flutter defined function
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         // return object of type Dialog
//         return AlertDialog(
//           title: Text('Hacer un nuevo pedido'),
//           content: Column(
//             // child: ListBody(
//             children: <Widget>[
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceAround,
//                 children: <Widget>[
//                   FlatButton(
//                     onPressed: () {
//                       setState(() {
//                         cantidad++;
//                       });
//                     },
//                     child: Icon(Icons.add),
//                   ),
//                   Container(
//                     width: 40,
//                     child: TextField(
//                       decoration: InputDecoration(
//                           hintText: cantidad.toString(),
//                           labelStyle: TextStyle(color: Colors.grey),
//                           focusedBorder: UnderlineInputBorder(
//                               borderSide:
//                                   BorderSide(color: Colors.amber, width: 2))),
//                       cursorColor: Colors.amber,
//                       keyboardType: TextInputType.text,
//                     ),
//                   ),
//                   FlatButton(
//                     onPressed: () {},
//                     child: Icon(Icons.maximize),
//                   ),
//                 ],
//               ),
//               Row(
//                 children: <Widget>[
//                   GestureDetector(
//                     onTap: () {},
//                     child: Container(
//                       height: 10,
//                       width: 10,
//                       // color: _value == 0 ? Colors.grey : Colors.transparent,
//                       child: Icon(Icons.call),
//                     ),
//                   ),
//                   SizedBox(width: 4),
//                   GestureDetector(
//                     onTap: () {},
//                     child: Container(
//                       height: 10,
//                       width: 10,
//                       // color: _value == 1 ? Colors.grey : Colors.transparent,
//                       child: Icon(Icons.message),
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//             // ),
//           ),
//           actions: <Widget>[
//             FlatButton(
//               child: Text('Approve'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }
  

