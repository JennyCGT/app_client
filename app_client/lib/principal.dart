import 'package:flutter/material.dart';
import 'package:app_client/Inicio.dart' as first;
import 'package:app_client/mapa.dart' as second;
import 'package:app_client/novedades.dart' as third;
import 'package:app_client/chatbot.dart' as fourth; 
import 'package:flutter/rendering.dart';
import 'package:app_client/drawe_principal.dart' as drawer;
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'dart:async';

class Principal extends StatefulWidget {
  @override
  _Principal createState() => _Principal();
}

class _Principal extends State<Principal> {
  get crossAxisAlignment => null;
  String _scanBarcode = 'Unknown';

  TabController _controller;

  @override
  void initState() {
    super.initState();
    // _controller = new TabController(length: 3, vsync: this  );
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
      print(_scanBarcode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: choices.length,
        child: Scaffold(
          drawer: Drawer(
            child: drawer.Drawerprincipal(),
          ),
          appBar: AppBar(
            backgroundColor: Color(0xff00574b),
            //  centerTitle: true,
            title: const Text('ClickGas'),

            bottom: TabBar(
              isScrollable: true,
              controller: _controller,
              labelColor: Colors.amber,
              unselectedLabelColor: Colors.grey,
              indicatorColor: Colors.amber,
              tabs: choices.map((Choice choice) {
                return Tab(
                  text: choice.title,
                  icon: Icon(choice.icon),
                );
              }).toList(),
            ),
          ),
          body: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                new first.Screen(),
                new second.Screen(),
                new third.Screen(),
                new fourth.Screen()
                
              ]),
          floatingActionButton: FloatingActionButton(
            onPressed:  () => scanQR(),
            child: Icon(Icons.scanner),
            backgroundColor: Colors.green,
          ),
        ),
      ),
    );
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'INICIO', icon: Icons.home),
  const Choice(title: 'MAPA', icon: Icons.gps_fixed),
  const Choice(title: 'NOVEDADES', icon: Icons.format_list_bulleted),
  const Choice(title: 'CHAT BOT', icon: Icons.chat),
];
